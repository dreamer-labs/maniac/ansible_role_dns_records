ansible_role_dns_records
=========

Role for provisioning DNS records to different providers.

Role Variables
--------------

   Currently the only supported provider is cloudflare

```
dns_provider: "cloudflare"
```

Username for cloud flare

```
dns_cloudflare_account_email: test@example.com
```

Cloudflare Global API token. This can't be one of the user specified tokens

```
dns_cloudflare_api_token: thisisnotavalidtoken
```

Dictionary for defining the records to be created, see the defaults for more options

```
dns:
  - zone: "example.com"
    record: "test"
    type: "A"
    ttl: 120
    value: "8.8.8.8"
```

Example Playbook
----------------

```- hosts: servers
  roles:
     - { role: ansible-role-security, security: role_security }
```

License
-------

MIT

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
